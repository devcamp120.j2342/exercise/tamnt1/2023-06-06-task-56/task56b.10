
package main.java.com.example.circle.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.restapi.Model.Book;
import com.example.restapi.Service.BookService;

import main.java.com.example.circle.Service.Service;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class MainController {
    @Autowired
    private Service service;

    @GetMapping("/cirlce-area")
    public double getCircleArea() {
        return service.createCircle();
    }

    @GetMapping("/cylinder-volumne")
    public double getCylinderVolumne(double radius, double height) {
        return service.createCylinder(radius, height);
    }

}
