package main.java.com.example.circle.Service;

import main.java.com.example.circle.Model.Circle;

public class Service {

    public double createCircle() {
        Circle circle = new Circle(2.0);
        return circle.getArea();
    }

    public double createCylinder(double radius, double height) {
        Cylinder cylinder = new Cylinder(3.0, 10.0);
        return cylinder.getVolume();
    }
}
